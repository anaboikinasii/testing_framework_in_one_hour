import os
import pytest

from tests.test_base import TestBase


class TestUploadFile(TestBase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        with open("{}/test_data/cat.jpg".format(os.path.dirname(os.path.abspath(__file__))), "rb") as cat_file:
            cls.cat_file_contents = cat_file.read()

    @pytest.allure.feature("Upload file")
    def test_upload_kitten(self):
        self.step("Upload cat.jpg file")
        self.dropbox_client.files.create(self.cat_file_contents, "/cat.jpg")
        self.step("List root folder")
        folder_obj = self.dropbox_client.folders.list("")
        self.step("Find cat.jpg")
        for fs_obj in folder_obj.entries:
            if fs_obj.name == "cat.jpg":
                self.step("Verify cat.jpg has non 0 size")
                self.assertTrue(fs_obj.size > 0, "File has zero size")

    @pytest.allure.feature("Upload file")
    def test_upload_kitten_2(self):
        self.step("Upload cat.jpg file")
        self.dropbox_client.files.create(self.cat_file_contents, "/cat.jpg")
        self.step("List root folder")
        folder_obj = self.dropbox_client.folders.list("")
        self.step("Find cat.jpg")
        for fs_obj in folder_obj.entries:
            if fs_obj.name == "cat.jpg":
                self.step("Verify cat.jpg has non 0 size")
                self.assertTrue(fs_obj.size > 0, "File has zero size")
