import logging
import os
import unittest
import json

from api.dropbox_client import DropboxClient
from tool.steps import Steps


class TestBase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        path_to_config = os.environ.get("TEST_CONFIG")
        with open(path_to_config) as config_file:
            self.token = json.loads(config_file.read())["token"]
        self._cleanup_objects = []
        self.log = logging.getLogger(self.id())
        self.steps = Steps(self.log)
        self.step = self.steps.step
        self.precondition = self.steps.pre_condition
        self.cleanup = self.steps.cleanup
        self.dropbox_client = DropboxClient(self._cleanup_objects, self.token)

    def tearDown(self):
        super().tearDown()
        for obj in self._cleanup_objects:
            self.cleanup("Removing {}".format(obj['path']))
            obj["action"](obj["path"])
