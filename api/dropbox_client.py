import dropbox

from api.drobpox_files import DropboxFiles
from api.dropbox_folders import DropboxFolders


class DropboxClient(object):
    def __init__(self, cleanup_objects, token):
        # todo: put toke into config
        self._client = dropbox.Dropbox(token)
        self.files = DropboxFiles(self._client, cleanup_objects)
        self.folders = DropboxFolders(self._client, cleanup_objects)