from api.dropbox_fs_base import DropboxFsBase


class DropboxFiles(DropboxFsBase):
    def __init__(self, client, cleanup_objects):
        super().__init__(client)
        self._cleanup_objects = cleanup_objects

    def create(self, file_data, file_path):
        self._client.files_upload(f=file_data, path=file_path)
        self._cleanup_objects.append({
            "path": file_path,
            "action": self.delete
        })
