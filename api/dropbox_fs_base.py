

class DropboxFsBase(object):
    def __init__(self, client):
        self._client = client

    def delete(self, path):
        self._client.files_delete_v2(path)

