from api.dropbox_fs_base import DropboxFsBase


class DropboxFolders(DropboxFsBase):
    def __init__(self, client, cleanup_objects):
        super().__init__(client)
        self._cleanup_objects = cleanup_objects

    def create(self):
        pass

    def list(self, path):
        return self._client.files_list_folder(path)
