import pytest


class Steps(object):

    def __init__(self, logger):
        self._log = logger
        self._i = 1

    def step(self, msg):
        with pytest.allure.step("Step #{}: {}".format(self._i, msg)):
            self._log.info("Step #{}: {}".format(self._i, msg))
            self._i += 1

    def pre_condition(self, msg):
        with pytest.allure.step(msg):
            self._log.info(msg)

    def cleanup(self, msg):
        with pytest.allure.step(msg):
            self._log.info(msg)
